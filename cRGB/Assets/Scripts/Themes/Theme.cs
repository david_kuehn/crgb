﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Theme
{
    public string themeName;

    [Space]

    public int price;

    [Space]

    public Color backgroundColor;

    [Space]

    public Sprite emptyBoxImage;
    public Sprite redBoxImage;
    public Sprite greenBoxImage;
    public Sprite blueBoxImage;

    //Class Constructor
    public Theme()
    {
        //Constructor Stuff
    }
}
