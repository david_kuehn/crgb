﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeManager : MonoBehaviour {

    public List<Theme> purchasedThemes;

    public List<Theme> themes;

    public Theme currentTheme;

    public static ThemeManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        currentTheme = themes[PlayerPrefs.GetInt("CurrentThemeNumber")];
        SetActiveTheme(currentTheme.themeName, null);

        LevelSaveManager.LoadAndSetPurchasedThemes(Application.persistentDataPath + "/StreamingAssets/ThemeData/purchasedthemes.txt");
    }

    public void SetActiveTheme(string themeName, Text currentThemeText)
    {
        for (int i = 0; i < themes.Count; i++)
        {
            if (themes[i].themeName == themeName)
            {
                currentTheme = themes[i];
                PlayerPrefs.SetInt("CurrentThemeNumber", i);
            }
        }

        Camera.main.backgroundColor = currentTheme.backgroundColor;

        if (currentThemeText != null)
        {
            currentThemeText.text = currentTheme.themeName;
        }
    }

    public void SetThemeLevelProperties(List<GameObject> boxesToSet)
    {
        BoxManager currentLevelBoxManager = null;
        VSBoxManager currentVSBoxManager = null;

        if (GameObject.FindGameObjectWithTag("Level") != null)
        {
            currentLevelBoxManager = GameObject.FindGameObjectWithTag("Level").GetComponent<BoxManager>();

            foreach (GameObject go in currentLevelBoxManager.dynamicBoxPositions)
            {
                if (go.name.Contains("Red"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.redBoxImage;
                }

                else if (go.name.Contains("Green"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.greenBoxImage;
                }

                else if (go.name.Contains("Blue"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.blueBoxImage;
                }

                else if (go.name.Contains("Empty"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.emptyBoxImage;
                }
            }

            foreach (GameObject go in boxesToSet)
            {
                if (go.name.Contains("Red"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.redBoxImage;
                }

                else if (go.name.Contains("Green"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.greenBoxImage;
                }

                else if (go.name.Contains("Blue"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.blueBoxImage;
                }

                else if (go.name.Contains("Empty"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.emptyBoxImage;
                }
            }
        }

        else if (GameObject.FindGameObjectWithTag("Versus") != null)
        {
            currentVSBoxManager = GameObject.FindGameObjectWithTag("Versus").GetComponent<VSBoxManager>();

            foreach (GameObject go in currentVSBoxManager.dynamicBoxPositions)
            {
                if (go.name.Contains("Red"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.redBoxImage;
                }

                else if (go.name.Contains("Green"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.greenBoxImage;
                }

                else if (go.name.Contains("Blue"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.blueBoxImage;
                }

                else if (go.name.Contains("Empty"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.emptyBoxImage;
                }
            }

            foreach (GameObject go in boxesToSet)
            {
                if (go.name.Contains("Red"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.redBoxImage;
                }

                else if (go.name.Contains("Green"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.greenBoxImage;
                }

                else if (go.name.Contains("Blue"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.blueBoxImage;
                }

                else if (go.name.Contains("Empty"))
                {
                    go.GetComponent<SpriteRenderer>().sprite = currentTheme.emptyBoxImage;
                }
            }
        }
    }

    public static Theme GetTheme(string themeToGetName)
    {
        foreach (Theme theme in ThemeManager.instance.themes)
        {
            if (theme.themeName == themeToGetName)
            {
                return theme;
            }
        }

        Debug.LogError("Theme with name: " + themeToGetName + " could not be found.");
        return null;
    }
}