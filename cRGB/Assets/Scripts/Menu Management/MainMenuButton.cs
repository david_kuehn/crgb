﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuButton : MonoBehaviour {

    private MenuManager menuManagerScript;
    private LevelPauseManager levelPauseManagerScript;

    public GameObject mainMenuPrefab;

    private void Start()
    {
        GameObject menuManagerHost = GameObject.FindGameObjectWithTag("GameManager");
        menuManagerScript = menuManagerHost.GetComponent<MenuManager>();
        gameObject.GetComponent<Button>().onClick.AddListener(LoadMainMenu);

        levelPauseManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<LevelPauseManager>();
    }

    void LoadMainMenu()
    {
        menuManagerScript.UnloadMenu(transform.parent.gameObject);

        LevelSaveManager.SaveLevel(LevelPauseManager.instance.levels[SaveManager.instance.GetBoxManagerLevelNumber(GameObject.FindGameObjectWithTag("Level"))], Application.persistentDataPath + "/StreamingAssets/LevelData/", "LevelData.txt");

        menuManagerScript.LoadMenu(mainMenuPrefab);
        levelPauseManagerScript.ClearLoadedLevel();
        levelPauseManagerScript.startResumeButton = GameObject.Find("StartResumeButton");

        //menuManagerScript.UnloadMenu(transform.parent.gameObject);
    }
}
