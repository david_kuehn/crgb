﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopUpMenuManager : MonoBehaviour {

    public string headerTextKeyID;
    public string mainTextKeyID;
    public TextMeshProUGUI headerText;
    public TextMeshProUGUI popUpText;

    private void Awake()
    {
        headerText.text = RemoteSettings.GetString(headerTextKeyID);
        popUpText.text = RemoteSettings.GetString(mainTextKeyID);
    }
}
