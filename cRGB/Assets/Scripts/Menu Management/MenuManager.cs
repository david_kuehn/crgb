﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class MenuManager : MonoBehaviour {

    public LevelPauseManager levelPauseManagerScript;

    [Space]
    public GameObject mainMenuPrefab;
    public GameObject popUpPrefab;

    private GameObject canvas;
    private GameObject pausedPanelMenu;

    private bool gameWasStarted;

    public static MenuManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas");

        GameObject newMainMenu = Instantiate(mainMenuPrefab, canvas.transform);
        newMainMenu.GetComponent<PlayableDirector>().Play();
    }

    private void Update()
    {
        CurrencyManager.instance.UpdateAmountOfCoins();
    }

    public void LoadMenu(GameObject menu)
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas");

        GameObject newInstantiatedMenu = Instantiate(menu, canvas.transform);

        if (menu.tag == "Pause Menu")
        {
            pausedPanelMenu = newInstantiatedMenu;
            levelPauseManagerScript.pausedPanel = newInstantiatedMenu;
            newInstantiatedMenu.SetActive(false);
        }
    }

    public void UnloadMenu(GameObject menu)
    {
        Destroy(menu);
    }

    public void PauseUnpause(bool activeState)
    {
        if (pausedPanelMenu == null)
        {
            for (int i = 0; i < canvas.transform.childCount; i++)
            {
                if (canvas.transform.GetChild(i).tag == "Pause Menu")
                {
                    pausedPanelMenu = canvas.transform.GetChild(i).gameObject;
                }
            }
        }

        pausedPanelMenu.SetActive(activeState);
    }

    public void DestroyPauseMenu()
    {
        for (int i = 0; i < canvas.transform.childCount; i++)
        {
            if (canvas.transform.GetChild(i).tag == "Pause Menu")
            {
                Destroy(canvas.transform.GetChild(i).gameObject);
                pausedPanelMenu = null;
            }
        }
    }
}
