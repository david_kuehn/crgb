﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeSelector : MonoBehaviour {

    public GameObject buttonPrefab;
    public GameObject contentParent;
    public Text currentThemeText;

    ThemeManager themeManagerScript = ThemeManager.instance;

    private List<GameObject> themeButtons = new List<GameObject>();

    #region Singleton

    public static ThemeSelector instance = null;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    private void Start()
    {
        foreach (Theme theme in themeManagerScript.themes)
        {
            GameObject instantiatedButton = Instantiate(buttonPrefab, contentParent.transform);
            instantiatedButton.name = theme.themeName + " Button";
            instantiatedButton.transform.GetChild(0).GetComponent<Text>().text = theme.themeName;

            instantiatedButton.GetComponent<Button>().onClick.AddListener(() => themeManagerScript.SetActiveTheme(theme.themeName, currentThemeText));

            themeButtons.Add(instantiatedButton);
        }

        UpdateThemeButtons();
        currentThemeText.text = themeManagerScript.currentTheme.themeName;
    }

    private void Update()
    {
        contentParent.GetComponent<Image>().color = Camera.main.backgroundColor;
    }

    private void UpdateThemeButtons()
    {
        bool hasFlaggedAsInteractable = false;

        Debug.Log(themeButtons.Count);

        foreach (GameObject button in themeButtons)
        {
            if (button.transform.Find("ThemeNameText").GetComponent<Text>().text != "Default")
            {
                foreach (Theme purchasedTheme in themeManagerScript.purchasedThemes)
                {
                    if (button.name.Contains(purchasedTheme.themeName))
                    {
                        button.GetComponent<Button>().interactable = true;

                        hasFlaggedAsInteractable = true;

                        Debug.Log(button.name + button.GetComponent<Button>().interactable);
                    }
                }

                if (!hasFlaggedAsInteractable)
                {
                    button.GetComponent<Button>().interactable = false;
                }
            }

            else
            {
                button.GetComponent<Button>().interactable = true;
            }
        }
    }
}
