﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsButton : MonoBehaviour {

    private MenuManager menuManagerScript;
    private LevelPauseManager levelPauseManagerScript;

    public GameObject settingsMenuPrefab;

    private void Start()
    {
        GameObject menuManagerHost = GameObject.FindGameObjectWithTag("GameManager");
        menuManagerScript = menuManagerHost.GetComponent<MenuManager>();
        gameObject.GetComponent<Button>().onClick.AddListener(LoadSettingsMenu);

        levelPauseManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<LevelPauseManager>();
    }

    void LoadSettingsMenu()
    {
        menuManagerScript.UnloadMenu(transform.parent.gameObject);

        LevelSaveManager.SaveLevel(LevelPauseManager.instance.levels[SaveManager.instance.GetBoxManagerLevelNumber(GameObject.FindGameObjectWithTag("Level"))], Application.persistentDataPath + "/StreamingAssets/LevelData/", "LevelData.txt");

        menuManagerScript.LoadMenu(settingsMenuPrefab);
        levelPauseManagerScript.ClearLoadedLevel();
    }
}
