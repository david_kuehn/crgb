﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextLevelButton : MonoBehaviour {

    private Button thisButton;

    private void Start()
    {
        thisButton = GetComponent<Button>();
        thisButton.onClick.AddListener(AdvanceToNextLevel);

        if (PlayerPrefs.GetInt("LastLevelSolved") >= SaveManager.instance.GetBoxManagerLevelNumber(GameObject.FindGameObjectWithTag("Level")) && !GameObject.FindGameObjectWithTag("Level").GetComponent<BoxManager>().isTutorial)
        {
            thisButton.interactable = true;
        }

        else if (SaveManager.instance.GetBoxManagerLevelNumber(GameObject.FindGameObjectWithTag("Level")) == 0)
        {
            this.transform.GetChild(0).GetComponent<Text>().text = "Skip Tutorial";
            thisButton.interactable = true;
        }

        else
        {
            thisButton.interactable = false;
        }
    }

    void AdvanceToNextLevel()
    {
        if (SaveManager.instance.GetBoxManagerLevelNumber(GameObject.FindGameObjectWithTag("Level")) != 0)
        {
            LevelSaveManager.SaveLevel(LevelPauseManager.instance.levels[SaveManager.instance.GetBoxManagerLevelNumber(GameObject.FindGameObjectWithTag("Level"))], Application.persistentDataPath + "/StreamingAssets/LevelData/", "LevelData.txt");

            LevelPauseManager.instance.LoadNextLevel();
        }

        else
        {
            PlayerPrefs.SetInt("TutorialIsSolved", 1);
            LevelPauseManager.instance.LoadNextLevel();
        }
    }
}
