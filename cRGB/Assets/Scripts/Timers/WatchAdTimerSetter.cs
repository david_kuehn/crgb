﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Timers;

public class WatchAdTimerSetter : MonoBehaviour {

    public Text watchAdText;
    public Button watchAdButton;

    void Update()
    {
        if (!WatchAdTimerManager.instance.timerIsDone)
        {
            var timeRemainingMinutesSeconds = TimeSpan.FromSeconds(TimersManager.RemainingTime(WatchAdTimerManager.instance.AdTimer));
            string timeToPrint = string.Format("{0}:{1:00}", (int)timeRemainingMinutesSeconds.TotalMinutes, timeRemainingMinutesSeconds.Seconds);

            watchAdText.text = "Watch Ad in " + timeToPrint;
            watchAdButton.interactable = false;
        }

        else
        {
            watchAdText.text = "Watch Ad";
            watchAdButton.interactable = true;
        }
    }
}
