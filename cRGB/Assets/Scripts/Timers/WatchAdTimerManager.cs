﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Timers;

public class WatchAdTimerManager : MonoBehaviour
{
    public static WatchAdTimerManager instance = null;

    public float timerDefaultLength;
    private float timerCurrentLength;

    [HideInInspector]
    public bool timerIsDone;

    void Awake()
    {
        #region Singleton
        if (instance == null)
        {
            instance = this;
        }
        #endregion

        float lastLoggedInTimeSeconds = PlayerPrefs.GetFloat("TimeOfDayLastLoginSeconds");
        int lastLoggedInDay = PlayerPrefs.GetInt("DayOfLastLogin");
        int lastLoggedInMonth = PlayerPrefs.GetInt("MonthOfLastLogin");
        float lastTimerLength = PlayerPrefs.GetFloat("AdTimerTimeRemaining");

        if (PlayerPrefs.GetInt("TimerWasDoneOnExit") == 0)

        if (lastLoggedInTimeSeconds + timerDefaultLength > (float)DateTime.Now.TimeOfDay.TotalSeconds)
        {
            if (lastLoggedInMonth == DateTime.Now.Month)
            {
                if (lastLoggedInDay == DateTime.Now.Day)
                {
                    TimersManager.SetTimer(this, lastTimerLength - ((float)DateTime.Now.TimeOfDay.TotalSeconds - lastLoggedInTimeSeconds), AdTimer);
                    TimersManager.SetLoopableTimer(this, 1f, ShowTimeRemaining);
                }
            }
        }

        else
        {
            timerIsDone = true;
        }
    }

    private void Update()
    {
        if (TimersManager.RemainingTime(AdTimer) < 0f)
        {
            timerIsDone = true;
        }
    }

    public void AdTimer()
    {
        //TimersManager.ClearTimer(ShowTimeRemaining);

        timerIsDone = true;
    }

    public void ShowTimeRemaining()
    {
        var timeRemainingMinutesSeconds = TimeSpan.FromSeconds(TimersManager.RemainingTime(AdTimer));
        string timeToPrint = string.Format("{0}:{1:00}", (int)timeRemainingMinutesSeconds.TotalMinutes, timeRemainingMinutesSeconds.Seconds);

        Debug.Log(timeToPrint);
    }

    public void StartAdTimersAtDefaultLength()
    {
        timerIsDone = false;

        TimersManager.SetTimer(this, timerDefaultLength, AdTimer);
        TimersManager.SetLoopableTimer(this, 1f, ShowTimeRemaining);
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetFloat("TimeOfDayLastLoginSeconds", (float)DateTime.Now.TimeOfDay.TotalSeconds);
        PlayerPrefs.SetInt("DayOfLastLogin", DateTime.Now.Day);
        PlayerPrefs.SetInt("MonthOfLastLogin", DateTime.Now.Month);

        if (!timerIsDone)
        {
            PlayerPrefs.SetFloat("AdTimerTimeRemaining", TimersManager.RemainingTime(AdTimer));
            PlayerPrefs.SetInt("TimerWasDoneOnExit", 0);
        }

        else
        {
            PlayerPrefs.SetInt("TimerWasDoneOnExit", 1);
        }
    }
}
