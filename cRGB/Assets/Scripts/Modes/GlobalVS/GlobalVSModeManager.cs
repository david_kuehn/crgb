﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class GlobalVSModeManager : MonoBehaviour {

    public Transform canvas;
    public GameObject endScreen;
    public PlayableDirector _321Director;
    public PlayableDirector _321Prefab;

    public GlobalVSBoxManager boxManager;

    public int playerID;

    [HideInInspector]
    public bool roundIsOver = false;
    bool endPanelHasInstantiated = false;
    bool scoreHasBeenSent = false;

    [HideInInspector]
    public TimeSpan playerTime;

    GameObject instantiatedEndScreen = null;

    dreamloLeaderBoard leaderboard;

    private void OnApplicationQuit()
    {
        leaderboard.ClearPlayerScore(PlayerPrefs.GetInt("PlayerID").ToString());
    }

    private void Awake()
    {
        leaderboard = GameObject.FindGameObjectWithTag("GameManager").GetComponent<dreamloLeaderBoard>();

        GenerateOrFindPlayerID();

        //Set Canvas variable
        canvas = GameObject.FindGameObjectWithTag("Canvas").transform;

        //Start 321 Anim
        PlayableDirector new321 = Instantiate(_321Prefab.gameObject, canvas).GetComponent<PlayableDirector>();
        _321Director = new321;
        //_321Director.transform.parent = canvas;
        _321Director.Play();
    }

    private void Update()
    {
        //Sets variable and changes text
        if (GameObject.FindGameObjectWithTag("321Panel"))
        {
            _321Director = GameObject.FindGameObjectWithTag("321Panel").GetComponent<PlayableDirector>();

            _321Director.transform.Find("PlayerText").GetComponent<Text>().text = "STARTING IN";
        }

        //If anim is done, start round
        if (!boxManager.readyToInit)
        {
            if (_321Director != null)
            {
                if (_321Director.state != PlayState.Playing)
                {
                    boxManager.readyToInit = true;

                    Destroy(_321Director.gameObject);
                    _321Director = null;
                }
            }
        }

        //Handles the end of the round
        if (roundIsOver && !endPanelHasInstantiated)
        {
            boxManager.hasInitiatedLevel = false;
            boxManager.readyToInit = false;
            playerTime = boxManager.stopwatch.Elapsed;

            boxManager.ClearBoxes();
            instantiatedEndScreen = Instantiate(endScreen, canvas);

            endPanelHasInstantiated = true;
        }

        //Handles sending score to server
        if (roundIsOver && endPanelHasInstantiated && !scoreHasBeenSent)
        {
            instantiatedEndScreen.GetComponent<GlobalVSEndPanel>().SendPlayerScore(playerID, (float)Math.Round(playerTime.TotalSeconds, 1));

            scoreHasBeenSent = true;
        }
    }

    void GenerateOrFindPlayerID()
    {
        int playerIDLength = 6;

        //If the player doesn't already have a PlayerID
        if (!PlayerPrefs.HasKey("PlayerID") || PlayerPrefs.GetInt("PlayerID") == 0)
        {
            string earlyPlayerID = null;

            //Generate a PlayerID and save it
            for (int i = 0; i < playerIDLength; i++)
            {
                if (earlyPlayerID == "0")
                {
                    earlyPlayerID = UnityEngine.Random.Range(1, 10).ToString();
                }

                //Append random digit to ID
                else
                {
                    earlyPlayerID += UnityEngine.Random.Range(1, 10).ToString();
                }
            }

            playerID = Int32.Parse(earlyPlayerID);
            PlayerPrefs.SetInt("PlayerID", playerID);
        }

        //If a PlayerID exists, set it
        else
        {
            playerID = PlayerPrefs.GetInt("PlayerID");
        }
    }
}
