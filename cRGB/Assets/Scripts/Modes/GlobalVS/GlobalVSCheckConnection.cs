﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalVSCheckConnection : MonoBehaviour {

    public GameObject notConnectedText;
    public Button startButton;

    void Start()
    {
        StartCoroutine(checkInternetConnection((isConnected) => 
        {
            //If there isn't an internet connection
            if (!isConnected)
            {
                notConnectedText.SetActive(true);
                startButton.enabled = false;
            }
        }));
    }

    IEnumerator checkInternetConnection(Action<bool> action)
    {
        //Pings google.com

        WWW www = new WWW("http://dreamlo.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }
}
