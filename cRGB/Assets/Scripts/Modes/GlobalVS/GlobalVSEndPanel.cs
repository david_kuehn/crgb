﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using TMPro;

public class GlobalVSEndPanel : MonoBehaviour {

    public PlayableDirector initialAnimDirector;
    public GameObject processingText;

    public Text playerPlaceText;
    public Text playerScoreText;

    [HideInInspector]
    public string playerPlace = "";

    bool placeWasSet = false;

    dreamloLeaderBoard leaderboard;

    private void Awake()
    {
        initialAnimDirector = GetComponent<PlayableDirector>();
        playerPlaceText = transform.Find("NumberText").GetComponent<Text>();

        leaderboard = GameObject.FindGameObjectWithTag("GameManager").GetComponent<dreamloLeaderBoard>();

        playerPlaceText.text = "";

        StartCoroutine(DeleteScoreAfterSeconds());
    }

    private void Update()
    {
        if (playerPlace != "" && !placeWasSet)
        {
            playerPlaceText.text = GetOrdinal(Convert.ToInt32(playerPlace));

            placeWasSet = true;
        }
    }

    public void RemoveGlobalVersusBase()
    {
        Destroy(GameObject.FindGameObjectWithTag("GlobalVersus"));
    }

    public void SendPlayerScore(int playerID, float playerTime)
    {
        //Set text in-game to player's score in seconds
        playerScoreText.text = "IN <b><color=black>" + playerTime.ToString() + "</color></b> SECONDS";

        //Send player's score to the leaderboard, get player's place from the leaderboard
        leaderboard.AddScore(playerID.ToString(), Convert.ToInt32(playerTime * 10), transform);
    }

    public void ClearPlayerScoreFromLeaderboard()
    {
        leaderboard.ClearPlayerScore(PlayerPrefs.GetInt("PlayerID").ToString());
    }

    IEnumerator DeleteScoreAfterSeconds()
    {
        yield return new WaitForSeconds(75);
        ClearPlayerScoreFromLeaderboard();
    }

    public string GetOrdinal(int number)
    {
        string suffix = String.Empty;

        int ones = number % 10;
        int tens = (int)Math.Floor(number / 10M) % 10;

        if (tens == 1)
        {
            suffix = "th";
        }
        else
        {
            switch (ones)
            {
                case 1:
                    suffix = "st";
                    break;

                case 2:
                    suffix = "nd";
                    break;

                case 3:
                    suffix = "rd";
                    break;

                default:
                    suffix = "th";
                    break;
            }
        }
        return String.Format("{0}{1}", number, suffix);
    }
}
