﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class SaveManager : MonoBehaviour {

    public string[] keysToDelete;

    public GameObject areYouSurePanel;
    public GameObject settingsPanel;

    public static SaveManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    #region PlayerPrefs Functions
    //Handles loading Are You Sure menu
    public void ResetProgressButton()
    {
        GetComponent<MenuManager>().LoadMenu(areYouSurePanel);
        GetComponent<MenuManager>().UnloadMenu(GameObject.FindGameObjectWithTag("Settings Menu"));
    }

    //Handles resetting PlayerPrefs
    public void ResetProgress()
    {
        foreach (string keyName in keysToDelete)
        {
            PlayerPrefs.DeleteKey(keyName);
        }

        LevelSaveManager.ResetLevels(Application.persistentDataPath + "/StreamingAssets/LevelData/leveldata.txt");

        GetComponent<MenuManager>().LoadMenu(settingsPanel);
        GetComponent<MenuManager>().UnloadMenu(GameObject.FindGameObjectWithTag("Are You Sure Panel"));
    }

    //Handles unloading Are You Sure menu
    public void CancelButton()
    {
        GetComponent<MenuManager>().LoadMenu(settingsPanel);
        GetComponent<MenuManager>().UnloadMenu(GameObject.FindGameObjectWithTag("Are You Sure Panel"));
    }
    #endregion

    #region External Utility Functions

    public List<string> GetListOfGameObjectNames(List<GameObject> gameObjectsToConvert)
    {
        List<string> gameObjectNames = new List<string>();

        foreach (GameObject go in gameObjectsToConvert)
        {
            gameObjectNames.Add(go.name);
        }

        return gameObjectNames;
    }

    public int GetBoxManagerLevelNumber(GameObject level)
    {
        int levelNumberToReturn = 0;

        foreach (Component component in level.GetComponents<Component>())
        {
            //Finds BoxManager3x3 on Active Level in scene
            if (component.GetType() == typeof(BoxManager))
            {
                levelNumberToReturn = component.GetComponent<BoxManager>().levelNumber;
                break;
            }

            //Add compatibility for BoxManager4x4/5x5 in the future
        }

        return levelNumberToReturn;
    }

    #endregion
}

[System.Serializable]
public class LevelData
{
    public int levelNumber;
    public List<GameObject> dynamicBoxes;
}
