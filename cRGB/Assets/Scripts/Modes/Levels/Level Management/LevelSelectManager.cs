﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectManager : MonoBehaviour {

    public GameObject tutorialLevelCircle;

    [Space]

    public List<GameObject> levelCircles;

    private void Start()
    {
        int lastLevelCompleted = PlayerPrefs.GetInt("LastLevelSolved");

        for (int i = 0; i < levelCircles.Count; i++)
        {
            Button buttonComponent = levelCircles[i].GetComponent<Button>();

            if (i > lastLevelCompleted)
            {
                buttonComponent.interactable = false;
            }

            else
            {
                buttonComponent.interactable = true;
            }
        }
    }
}
