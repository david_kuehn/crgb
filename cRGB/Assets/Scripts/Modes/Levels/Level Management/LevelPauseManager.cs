﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelPauseManager : MonoBehaviour {

    public GameObject currentLevel;
    public List<GameObject> levelPrefabs;

    [Space]

    public List<GameObject> objectsToLoadOnLevelLoad;

    [Space]

    public MenuManager menuManagerScript;
    public GameObject startResumeButton;
    public GameObject pausedPanel;
    public Text levelNumberText;
    public string pauseMenuName;

    [Space]

    public GameObject pausedPanelPrefab;

    [Space]

    public GameObject levelOnePrefab;

    [Space]

    public GameObject tutorialLevelPrefab;

    [HideInInspector]
    public int currentLevelNumber;

    public bool isPaused = false;

    public LevelData[] levels;

    public static LevelPauseManager instance = null;

    public bool isFirstTimeBeingRun = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        if (PlayerPrefs.GetInt("FirstTime") != 1)
        {
            isFirstTimeBeingRun = true;

            PlayerPrefs.SetInt("FirstTime ", 1);
        }

        if (RemoteSettings.GetBool("CheckForNewFeature") == true && RemoteSettings.GetString("NewFeatureText") != PlayerPrefs.GetString("LastCachedRemote"))
        {
            StartCoroutine(NewFeaturePopUpOnStart());
            PlayerPrefs.SetString("LastCachedRemote", RemoteSettings.GetString("NewFeatureText"));
        }

        Application.targetFrameRate = 60;
    }

    IEnumerator NewFeaturePopUpOnStart()
    {
        yield return new WaitForSeconds(5f);
        menuManagerScript.LoadMenu(menuManagerScript.popUpPrefab);
    }
    
    private void OnApplicationQuit()
    {
        if (Application.isEditor)
        {
            //Ensures that PlayerPrefs are not taken when building
            PlayerPrefs.DeleteAll();
            LevelSaveManager.ClearAllSavedData();
            PlayerPrefs.SetInt("LastLevelSolved", -1);
        }
    }

    private void Update()
    {
        SetLevelNumber();
        LoadOtherLevelStuff();

        if (pausedPanel != null)
        {
            if (pausedPanel.activeSelf)
            {
                isPaused = true;
            }

            else
            {
                isPaused = false;
            }
        }

        else
        {
            isPaused = false;
        }

        if (GameObject.FindGameObjectWithTag("StartResumeButton") != null)
        {
            startResumeButton = GameObject.FindGameObjectWithTag("StartResumeButton");

            if (PlayerPrefs.GetInt("TutorialIsSolved") == 0)
            {
                startResumeButton.transform.GetChild(0).GetComponent<Text>().text = "Start Tutorial";
            }

            else
            {
                startResumeButton.transform.GetChild(0).GetComponent<Text>().text = "PLAY";
            }
        }
    }

    public void LoadNextLevel()
    {
        //If is the last level
        if (currentLevelNumber >= levelPrefabs.Count)
        {
            ClearLoadedLevel();
            menuManagerScript.LoadMenu(menuManagerScript.mainMenuPrefab);
        }

        else
        {
            LoadLevel(currentLevelNumber + 1);
        }       
    }

    public void LoadTutorialOrModesPanel(GameObject modesPanelPrefab)
    {
        menuManagerScript = GetComponent<MenuManager>();

        if (PlayerPrefs.GetInt("TutorialIsSolved") == 1)
        {
            menuManagerScript.LoadMenu(modesPanelPrefab);
        }

        else
        {
            GameObject tutorialLevel = Instantiate(tutorialLevelPrefab);

            currentLevel = tutorialLevel;

            menuManagerScript.LoadMenu(pausedPanelPrefab);
        }
    }
    public void SetLevelNumber()
    {
        GameObject[] levelsInScene = GameObject.FindGameObjectsWithTag("Level");
        List<GameObject> levelsActive = new List<GameObject>();

        if (levelsInScene.Length != 0)
        {            
            foreach (GameObject level in levelsInScene)
            { 
                if (level.activeInHierarchy == true)
                {
                    levelsActive.Add(level);
                }

                if (levelsActive.Count == 1)
                {
                    if (level.GetComponent<BoxManager>().levelNumber == 0)
                    {
                        levelNumberText.text = "T";
                        currentLevelNumber = level.GetComponent<BoxManager>().levelNumber;
                    }

                    else
                    {
                        levelNumberText.text = level.GetComponent<BoxManager>().levelNumber.ToString();
                        currentLevelNumber = level.GetComponent<BoxManager>().levelNumber;
                    }                   
                }

                else
                {
                    Debug.LogWarning("There is more than one active Level in the Scene!");
                }
            }
        }
    }

    public bool LevelActiveInScene()
    {
        GameObject[] levelsInScene = GameObject.FindGameObjectsWithTag("Level");

        if (levelsInScene.Length >= 1)
        {
            return true;
        }

        return false;
    }

    public void ClearLoadedLevel()
    {
        if (currentLevel != null)
        {
            menuManagerScript.DestroyPauseMenu();

            Destroy(currentLevel);
        }

        else
        {
            menuManagerScript.DestroyPauseMenu();

            Destroy(GameObject.FindGameObjectWithTag("Level"));
        }
    }

    public void LoadLevel(int levelNumber)
    {
        ClearLoadedLevel();

        if (levelNumber == 0)
        {
            //Load Tutorial

            GameObject newInstantiatedTutorialLevel = Instantiate(tutorialLevelPrefab);
            currentLevel = newInstantiatedTutorialLevel;
            currentLevelNumber = 0;

            menuManagerScript.LoadMenu(pausedPanelPrefab);

            PlayerPrefs.SetInt("LastLevelActive", 0);
        }

        else
        {
            GameObject newInstantiatedLevel = Instantiate(levelPrefabs[levelNumber - 1]);

            newInstantiatedLevel.SetActive(true);
            currentLevel = newInstantiatedLevel;
            currentLevelNumber = newInstantiatedLevel.GetComponent<BoxManager>().levelNumber;

            menuManagerScript.LoadMenu(pausedPanelPrefab);

            AdManager.instance.AddToOrTriggerAd();

            PlayerPrefs.SetInt("LastLevelActive", newInstantiatedLevel.GetComponent<BoxManager>().levelNumber);
        }

        isPaused = false;
    }

    public void LoadOtherLevelStuff()
    {
        if (LevelActiveInScene())
        {
            foreach (GameObject go in objectsToLoadOnLevelLoad)
            {
                go.SetActive(true);
            }
        }

        else
        {
            foreach (GameObject go in objectsToLoadOnLevelLoad)
            {
                go.SetActive(false);
            }
        }
    }

    public void LoadVersusBase(GameObject versusBasePrefab)
    {
        Instantiate(versusBasePrefab);
    }
}
