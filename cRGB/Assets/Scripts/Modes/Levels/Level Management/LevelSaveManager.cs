﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public static class LevelSaveManager {

    public static void SaveLevel(LevelData levelToSave, string pathWithoutFileNameWithEndSlash, string fileName)
    {
        string shortPath = pathWithoutFileNameWithEndSlash;
        string longPath = pathWithoutFileNameWithEndSlash + fileName;

        //Create directory if it doesn't exist
        Directory.CreateDirectory(shortPath);

        //Create file if it doesn't exist
        if (!File.Exists(longPath))
        {
            File.Create(longPath).Close();
        }
        
        bool hasWritten = false;

        if (levelToSave.levelNumber != 0)
        {
            for (int i = 0; i < File.ReadAllLines(longPath).Length; i++)
            {
                if (File.ReadAllLines(longPath)[i].StartsWith(levelToSave.levelNumber.ToString()))
                {
                    int lineToEdit = i;

                    string[] oldFile = File.ReadAllLines(longPath);
                    string[] newFile = oldFile;

                    newFile[lineToEdit] = levelToSave.levelNumber.ToString() + "-" + CreateCSLFromList(SaveManager.instance.GetListOfGameObjectNames(levelToSave.dynamicBoxes));

                    File.WriteAllText(longPath, string.Empty);
                    File.WriteAllLines(longPath, newFile);
                    hasWritten = true;
                }
            }

            if (!hasWritten && levelToSave.levelNumber != 0)
            {
                File.AppendAllText(longPath, Environment.NewLine + levelToSave.levelNumber.ToString() + "-" + CreateCSLFromList(SaveManager.instance.GetListOfGameObjectNames(levelToSave.dynamicBoxes)));
            }

            else
            {
                File.AppendAllText(longPath, levelToSave.levelNumber.ToString() + "-" + CreateCSLFromList(SaveManager.instance.GetListOfGameObjectNames(levelToSave.dynamicBoxes)));

            }
        }
    }

    public static void SaveCSL(string pathWithoutFileNameWithEndSlash, string fileName, string CSLToSave)
    {
        //Create directory if it doesn't exist
        Directory.CreateDirectory(pathWithoutFileNameWithEndSlash);

        File.WriteAllText(pathWithoutFileNameWithEndSlash + fileName, CSLToSave);       
    }

    public static List<string> DeserializeCSL(string CSLToDeserialize)
    {
        List<string> deserializedCSL = new List<string>();

        string[] itemsSeparated = CSLToDeserialize.Split(',');

        foreach (string item in itemsSeparated)
        {
            deserializedCSL.Add(item);
        }

        return deserializedCSL;
    }

    public static List<string> LoadOrderOfDynamics(LevelData levelToLoad, string path)
    {
        List<string> orderOfDynamics = null;
        if (File.Exists(path))
        {
            foreach (string line in File.ReadAllLines(path))
            {
                if (line.StartsWith(levelToLoad.levelNumber.ToString()))
                {
                    orderOfDynamics = DeserializeLevelCSL(line);

                    break;
                }
            }
        }

        return orderOfDynamics;
    }

    public static void ResetLevels(string path)
    {
        File.WriteAllText(path, string.Empty);
    }

    private static string CreateCSLFromList(List<string> listToConvert)
    {
        string newCSL = null;

        foreach (string item in listToConvert)
        {
            if (newCSL == null)
            {
                newCSL = item;
            }

            else
            {
                newCSL = newCSL + "," + item;
            }
        }

        return newCSL;
    }

    private static List<string> DeserializeLevelCSL(string CSLtoDeserialize)
    {
        List<string> deserializedCSL = new List<string>();

        //Can be simplified, combine both expressions into string[] dynamicsSeparated
        string fullStringOfDynamics = CSLtoDeserialize.Split('-')[1];
        string[] dynamicsSeparated = fullStringOfDynamics.Split(',');
        
        foreach(string name in dynamicsSeparated)
        {
            deserializedCSL.Add(name);
        }

        return deserializedCSL;
    }

    public static void SavePurchasedThemes(string path)
    {
        List<Theme> purchasedThemesToSave = ThemeManager.instance.purchasedThemes;
        List<string> purchasedThemeNamesToSave = new List<string>();

        foreach (Theme theme in purchasedThemesToSave)
        {
            purchasedThemeNamesToSave.Add(theme.themeName);
        }

        SaveCSL(path, "purchasedthemes.txt", CreateCSLFromList(purchasedThemeNamesToSave));
    }

    public static void LoadAndSetPurchasedThemes(string path)
    {
        if (File.Exists(path))
        {
            List<string> purchasedThemeNames = DeserializeCSL(File.ReadAllText(path));
            ThemeManager themeManagerScript = ThemeManager.instance;

            foreach (string themeName in purchasedThemeNames)
            {
                foreach (Theme theme in themeManagerScript.themes)
                {
                    if (theme.themeName == themeName)
                    {
                        themeManagerScript.purchasedThemes.Add(theme);
                    }
                }
            }
        }
    }

    public static void ClearAllSavedData()
    {
        foreach (System.IO.FileInfo file in new DirectoryInfo(Application.persistentDataPath + "/StreamingAssets/").GetFiles()) file.Delete();
        foreach (System.IO.DirectoryInfo subDirectory in new DirectoryInfo(Application.persistentDataPath + "/StreamingAssets/").GetDirectories()) subDirectory.Delete(true);
    }
}
