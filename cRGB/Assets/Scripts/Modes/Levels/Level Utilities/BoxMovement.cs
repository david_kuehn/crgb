﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class BoxMovement : MonoBehaviour {

    public LevelPauseManager levelPauseManagerScript;

    public BoxManager boxManagerScript;
    
    public List<Collider2D> adjacentBoxColliders;

    public bool is4x4;

    bool isSolved;
    bool solvedGroupHasCycled;
    bool positionsHaveSwitched;

    bool hasVibrated = false;

    private void Awake()
    {
        levelPauseManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<LevelPauseManager>();

        boxManagerScript = transform.parent.gameObject.GetComponent<BoxManager>();
    }

    private void Start()
    {
        //If the level is above 15
        if (boxManagerScript.levelNumber > 15)
            is4x4 = true;
    }

    void Update()
    {
        if (!SwipeManager.instance.shouldSwipe)
        {
            SwitchPosition();
        }
        else
        {
            HandleSwipeResult(SwipeManager.instance.InGameSwipe());

            if (adjacentBoxColliders.Count == 0)
            {
                SetAdjacentBoxes();
            }
        }

        if (boxManagerScript.CheckIfSolved())
        {
            if (GameObject.FindGameObjectWithTag("SolvedGroup") == null && !solvedGroupHasCycled)
            {
                GameObject newSolvedGroup = Instantiate(boxManagerScript.solvedGroupPrefab);
                Destroy(newSolvedGroup, 3.2f);

                Analytics.CustomEvent("level_complete", new Dictionary<string, object>
                {
                    { "level_index", boxManagerScript.levelNumber }
                });

                solvedGroupHasCycled = true;

                if (PlayerPrefs.GetInt("LastLevelSolved") < boxManagerScript.levelNumber)
                {
                    CurrencyManager.instance.AddCoins(5);
                    PlayerPrefs.SetInt("LastLevelSolved", boxManagerScript.levelNumber);
                }
            }

            if (GameObject.FindGameObjectWithTag("SolvedGroup") == null && solvedGroupHasCycled)
            {
                LevelSaveManager.SaveLevel(LevelPauseManager.instance.levels[boxManagerScript.levelNumber], Application.persistentDataPath + "/StreamingAssets/LevelData/", "LevelData.txt");
                LevelPauseManager.instance.LoadNextLevel();
            }

            if (boxManagerScript.isTutorial)
            {
                PlayerPrefs.SetInt("TutorialIsSolved", 1);

                GameObject[] tutorialGameObjects = GameObject.FindGameObjectsWithTag("Tutorial");

                foreach (GameObject go in tutorialGameObjects)
                {
                    go.SetActive(false);
                }
            }

            if (!hasVibrated)
            {
                Handheld.Vibrate();
                hasVibrated = true;
            }
        }
    }

    //Click Detection
    void OnMouseDown()
    {
        if (!SwipeManager.instance.shouldSwipe)
        {
            SetAdjacentBoxes();
        }
    }

    public void SwitchPosition()
    {
        if (LevelPauseManager.instance.isPaused != true)
        {
            bool canClearList = false;

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = (Camera.main.ScreenPointToRay(Input.mousePosition));
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

                if (hit)
                {
                    foreach (Collider2D collider in adjacentBoxColliders)
                    {
                        if (hit.collider.gameObject == collider.gameObject)
                        {
                            Vector2 emptyBoxCurrentPos = transform.position;
                            Vector2 emptyBoxFuturePos = collider.transform.position;

                            transform.position = emptyBoxFuturePos;
                            collider.transform.position = emptyBoxCurrentPos;

                            foreach (Collider2D collider2 in adjacentBoxColliders)
                            {
                                collider2.gameObject.GetComponent<SpriteRenderer>().color = new Color(collider2.gameObject.GetComponent<SpriteRenderer>().color.r,
                            collider2.gameObject.GetComponent<SpriteRenderer>().color.g,
                            collider2.gameObject.GetComponent<SpriteRenderer>().color.b,
                            1f);
                            }

                            canClearList = true;
                        }
                    }
                }

            }

            if (canClearList)
            {
                adjacentBoxColliders.Clear();
            }
        }
    }

    public void HandleSwipeResult(SwipeDirection swipeResult)
    {
        bool canClearList = false;

        Ray targetRay = new Ray();
        Vector3 rayOrigin = new Vector3();

        switch (swipeResult)
        {
            case SwipeDirection.SwipeRight:
                Debug.Log(swipeResult);
                if (is4x4)
                    rayOrigin = new Vector3(transform.position.x + 1.2f, transform.position.y);
                else
                    rayOrigin = new Vector3(transform.position.x + Mathf.Abs(boxManagerScript.boxCoordinates[0].x), transform.position.y);
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.SwipeLeft:
                Debug.Log(swipeResult);
                if (is4x4)
                    rayOrigin = new Vector3(transform.position.x - 1.2f, transform.position.y);
                else
                    rayOrigin = new Vector3(transform.position.x - Mathf.Abs(boxManagerScript.boxCoordinates[0].x), transform.position.y);
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.SwipeUp:
                Debug.Log(swipeResult);
                if (is4x4)
                    rayOrigin = new Vector3(transform.position.x, transform.position.y + 1.2f);
                else
                    rayOrigin = new Vector3(transform.position.x, transform.position.y + Mathf.Abs(boxManagerScript.boxCoordinates[0].y));
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.SwipeDown:
                Debug.Log(swipeResult);
                if (is4x4)
                    rayOrigin = new Vector3(transform.position.x, transform.position.y - 1.2f);
                else
                    rayOrigin = new Vector3(transform.position.x, transform.position.y - Mathf.Abs(boxManagerScript.boxCoordinates[0].y));
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.NoSwipe:
                return;
        }

        Ray ray = targetRay;
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

        if (hit)
        {
            Debug.Log(hit.collider);
            foreach (Collider2D collider in adjacentBoxColliders)
            {
                if (hit.collider.gameObject == collider.gameObject)
                {
                    Vector2 emptyBoxCurrentPos = transform.position;
                    Vector2 emptyBoxFuturePos = collider.transform.position;

                    transform.position = emptyBoxFuturePos;
                    collider.transform.position = emptyBoxCurrentPos;

                    /*foreach (Collider2D collider2 in adjacentBoxColliders)
                    {
                        collider2.gameObject.GetComponent<SpriteRenderer>().color = new Color(collider2.gameObject.GetComponent<SpriteRenderer>().color.r,
                    collider2.gameObject.GetComponent<SpriteRenderer>().color.g,
                    collider2.gameObject.GetComponent<SpriteRenderer>().color.b,
                    1f);
                    }*/

                    canClearList = true;
                }
            }
        }

        if (canClearList)
        {
            adjacentBoxColliders.Clear();
        }
    }

    void SetAdjacentBoxes()
    {
        if (adjacentBoxColliders.Count == 0)
        {
            foreach (Collider2D collider in Physics2D.OverlapCircleAll(transform.position, 0.7f))
            {
                if (collider.gameObject != gameObject)
                {
                    adjacentBoxColliders.Add(collider);
                    if (!SwipeManager.instance.shouldSwipe)
                    {
                        collider.gameObject.GetComponent<SpriteRenderer>().color = new Color(collider.gameObject.GetComponent<SpriteRenderer>().color.r,
                        collider.gameObject.GetComponent<SpriteRenderer>().color.g,
                        collider.gameObject.GetComponent<SpriteRenderer>().color.b,
                        0.5f);
                    }
                }
            }
        }
    }
}