﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {

    public LevelPauseManager levelPauseManagerScript;
    private BoxMovement boxMovementScript;
    public Text tutorialText1;
    public Text tutorialText3;
    public string string1;
    public string string3;

    private bool tutorialIsTriggered = false;

    void Update()
    {
        if (!tutorialIsTriggered)
        {
            if (GameObject.FindGameObjectWithTag("Level") != null)
            {
                if (GameObject.FindGameObjectWithTag("Level").name.Contains(levelPauseManagerScript.tutorialLevelPrefab.name))
                {
                    TriggerTutorial();
                }
            }           
        }   

        if (GameObject.FindGameObjectWithTag("Level") != null)
        {
            if (!GameObject.FindGameObjectWithTag("Level").name.Contains(levelPauseManagerScript.tutorialLevelPrefab.name))
            {
                tutorialText1.gameObject.SetActive(false);
                tutorialText3.gameObject.SetActive(false);

                tutorialIsTriggered = false;
            }
        }

        else
        {
            tutorialText1.gameObject.SetActive(false);
            tutorialText3.gameObject.SetActive(false);

            tutorialIsTriggered = false;
        }
    }

    public void TriggerTutorial()
    {
        tutorialIsTriggered = true;

        tutorialText1.gameObject.SetActive(true);
        tutorialText3.gameObject.SetActive(true);

        //GameObject tutorialLevel = GameObject.FindGameObjectWithTag("Level");

        tutorialText1.text = string1;

        tutorialText3.text = string3;
    }
}
