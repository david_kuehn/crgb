﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class BoxManager : MonoBehaviour {

    public int levelNumber;

    public bool isTutorial;

    public GameObject solvedGroupPrefab;

    [Space]

    [Header("Dynamic Box Positions")]
    public List<GameObject> dynamicBoxPositions;

    [Header("Solution Box Positions")]
    public List<GameObject> solutionBoxPositions;

    [Header("Box Coordinates")]
    public List<Vector2> boxCoordinates;

    private bool dynamicsHaveBeenSet = false;
    private bool levelHasBeenSolved = false;

    [Header("Solution Renderer")]
    public int columns;
    public int boxesInColumn;
    public float downscaleFactor;

    public float ySpacing = .6f;

    public float[] startingXPositions;
    public float startingYPosition = -3.2f;

    private void Awake()
    {
        if (PlayerPrefs.GetInt("LastLevelSolved") >= levelNumber)
        {
            levelHasBeenSolved = true;
        }

        Analytics.CustomEvent("level_start", new Dictionary<string, object>
        {
            { "level_index", levelNumber }
        });

        dynamicsHaveBeenSet = false;
    }

    private void Start()
    {
        RenderSolution();

        ThemeManager.instance.SetThemeLevelProperties(dynamicBoxPositions);
    }

    private void Update()
    {
        UpdateDynamicBoxes();
    }

    public void UpdateDynamicBoxes()
    {
        if (dynamicsHaveBeenSet)
        {
            for (int i = 0; i < dynamicBoxPositions.Count; i++)
            {
                Ray ray = new Ray(new Vector3(boxCoordinates[i].x, boxCoordinates[i].y, -1), Vector3.back);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

                dynamicBoxPositions[i] = hit.collider.gameObject;
            }

            LevelPauseManager.instance.levels[levelNumber].dynamicBoxes = dynamicBoxPositions;
        }

        if (!dynamicsHaveBeenSet)
        {
            //Gets order of dynamics in string form
            if (!levelHasBeenSolved)
            {
                List<string> dynamicBoxNames = LevelSaveManager.LoadOrderOfDynamics(LevelPauseManager.instance.levels[levelNumber], Application.persistentDataPath + "/StreamingAssets/LevelData/LevelData.txt");

                if (dynamicBoxNames != null)
                {
                    //Gets GameObject with the corresponding name
                    for (int i = 0; i < dynamicBoxNames.Count; i++)
                    {
                        dynamicBoxPositions[i] = transform.Find(dynamicBoxNames[i]).gameObject;
                        dynamicBoxPositions[i].transform.position = new Vector2(boxCoordinates[i].x, boxCoordinates[i].y);
                    }

                    Debug.Log(dynamicBoxNames.Count);
                }
            }

            dynamicsHaveBeenSet = true;
        }

        for (int i = 0; i < dynamicBoxPositions.Count; i++)
        {
            Ray ray = new Ray(new Vector3(boxCoordinates[i].x, boxCoordinates[i].y, -1), Vector3.back);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

            dynamicBoxPositions[i] = hit.collider.gameObject;
        }

        LevelPauseManager.instance.levels[levelNumber].dynamicBoxes = dynamicBoxPositions;
    }

    public bool CheckIfSolved()
    {
        //for (int i = 0; i < solutionBoxPositions.Count; i++)
        //{
        //    if (dynamicBoxPositions[i].GetComponent<SpriteRenderer>().color != solutionBoxPositions[i].GetComponent<SpriteRenderer>().color)
        //    {
        //        return false;
        //    }
        //}
        //return true;

        
        for (int i = 0; i < solutionBoxPositions.Count; i++)
        {
            //Makes a string of the first four characters of the name of the Box currently being iterated
            string firstFourCharactersString = solutionBoxPositions[i].name.ToCharArray(0, 4)[0].ToString() +
                solutionBoxPositions[i].name.ToCharArray(0, 4)[1].ToString() +
                solutionBoxPositions[i].name.ToCharArray(0, 4)[2].ToString() +
                solutionBoxPositions[i].name.ToCharArray(0, 4)[3].ToString();

            //If the corresponding Dynamic Box doesn't contain the name of the current Solution Box, then return false
            if (!dynamicBoxPositions[i].name.Contains(firstFourCharactersString))
            {
                return false;
            }
        }

        return true;
    }

    private void RenderSolution()
    {
        GameObject solutionParent = new GameObject("SolutionParent");
        solutionParent.transform.parent = this.transform;

        List<GameObject> solutionBoxes = new List<GameObject>();

        int currentColumn = 0;

        for (int i = 0; i < columns; i++)
        {
            for (int x = 0; x < boxesInColumn; x++)
            {
                GameObject objectJustInstantiated = Instantiate(solutionBoxPositions[x + (i * boxesInColumn)], solutionParent.transform);
                objectJustInstantiated.transform.localScale = new Vector3(objectJustInstantiated.transform.localScale.x * downscaleFactor, objectJustInstantiated.transform.localScale.y * downscaleFactor, 1);
                objectJustInstantiated.transform.position = new Vector3(startingXPositions[currentColumn], startingYPosition + (ySpacing * -x));

                solutionBoxes.Add(objectJustInstantiated);

                if (objectJustInstantiated.tag == "EmptyBox")
                {
                    Destroy(objectJustInstantiated.GetComponent<BoxMovement>());
                    Destroy(objectJustInstantiated.GetComponent<BoxCollider2D>());

                    objectJustInstantiated.tag = "Untagged";
                }

                if (objectJustInstantiated.tag == "ColorBox")
                {
                    Destroy(objectJustInstantiated.GetComponent<BoxCollider2D>());

                    objectJustInstantiated.tag = "Untagged";
                }
            }

            currentColumn += 1;
        }

        ThemeManager.instance.SetThemeLevelProperties(solutionBoxes);
    }
}
