﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class VSEndPanel : MonoBehaviour {

    public PlayableDirector initialAnimDirector;
    public PlayableDirector secondAnimDirector;

    public GameObject playerOneText;
    public GameObject playerTwoText;

    private void Awake()
    {
        initialAnimDirector = GetComponent<PlayableDirector>();
    }

    private void Update()
    {
        if (initialAnimDirector.state == PlayState.Playing)
        {
            playerOneText.SetActive(true);
            playerTwoText.SetActive(true);
        }
    }

    public void RemoveVersusBase()
    {
        Destroy(GameObject.FindGameObjectWithTag("Versus"));
    }
}
