﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSBoxMovement : MonoBehaviour {

    public LevelPauseManager levelPauseManagerScript;

    public VSBoxManager boxManagerScript;
    public VSModeManager modeManagerScript;
    
    public List<Collider2D> adjacentBoxColliders = new List<Collider2D>();

    bool isSolved;
    bool solvedGroupHasCycled;
    bool positionsHaveSwitched;

    bool hasVibrated = false;

    private void Awake()
    {
        levelPauseManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<LevelPauseManager>();

        boxManagerScript = transform.parent.gameObject.GetComponent<VSBoxManager>();
        modeManagerScript = transform.parent.gameObject.GetComponent<VSModeManager>();
    }

    void Update()
    {
        if (!SwipeManager.instance.shouldSwipe)
        {
            SwitchPosition();
        }
        else
        {
            HandleSwipeResult(SwipeManager.instance.InGameSwipe());

            if (adjacentBoxColliders.Count == 0)
            {
                SetAdjacentBoxes();
            }
        }

        if (boxManagerScript.CheckIfSolved())
        {
            if (GameObject.FindGameObjectWithTag("SolvedGroup") == null && !solvedGroupHasCycled)
            {
                //REMOVE

                GameObject newSolvedGroup = Instantiate(boxManagerScript.solvedGroupPrefab);
                Destroy(newSolvedGroup, 3.2f);
                solvedGroupHasCycled = true;
            }

            //What to do after Solved animation is shown
            if (GameObject.FindGameObjectWithTag("SolvedGroup") == null && solvedGroupHasCycled)
            {
                modeManagerScript.playerIsDone += 1;
            }

            if (!hasVibrated)
            {
                Handheld.Vibrate();
                hasVibrated = true;
            }
        }
    }

    //Click Detection
    void OnMouseDown()
    {
        if (!SwipeManager.instance.shouldSwipe)
        {
            SetAdjacentBoxes();
        }
    }

    public void SwitchPosition()
    {
        if (levelPauseManagerScript.isPaused != true)
        {
            bool canClearList = false;

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = (Camera.main.ScreenPointToRay(Input.mousePosition));
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

                if (hit)
                {
                    foreach (Collider2D collider in adjacentBoxColliders)
                    {
                        if (hit.collider.gameObject == collider.gameObject)
                        {
                            Vector2 emptyBoxCurrentPos = transform.position;
                            Vector2 emptyBoxFuturePos = collider.transform.position;

                            transform.position = emptyBoxFuturePos;
                            collider.transform.position = emptyBoxCurrentPos;

                            foreach (Collider2D collider2 in adjacentBoxColliders)
                            {
                                collider2.gameObject.GetComponent<SpriteRenderer>().color = new Color(collider2.gameObject.GetComponent<SpriteRenderer>().color.r,
                            collider2.gameObject.GetComponent<SpriteRenderer>().color.g,
                            collider2.gameObject.GetComponent<SpriteRenderer>().color.b,
                            1f);
                            }

                            canClearList = true;
                        }
                    }
                }

            }

            if (canClearList)
            {
                adjacentBoxColliders.Clear();
            }
        }
    }

    public void HandleSwipeResult(SwipeDirection swipeResult)
    {
        bool canClearList = false;

        Ray targetRay = new Ray();
        Vector3 rayOrigin = new Vector3();

        switch (swipeResult)
        {
            case SwipeDirection.SwipeRight:
                rayOrigin = new Vector3(transform.position.x + Mathf.Abs(boxManagerScript.boxCoordinates[0].x), transform.position.y);
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.SwipeLeft:
                rayOrigin = new Vector3(transform.position.x - Mathf.Abs(boxManagerScript.boxCoordinates[0].x), transform.position.y);
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.SwipeUp:
                rayOrigin = new Vector3(transform.position.x, transform.position.y + Mathf.Abs(boxManagerScript.boxCoordinates[0].y));
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.SwipeDown:
                rayOrigin = new Vector3(transform.position.x, transform.position.y - Mathf.Abs(boxManagerScript.boxCoordinates[0].y));
                targetRay = new Ray(rayOrigin, transform.forward);
                break;

            case SwipeDirection.NoSwipe:
                return;
        }

        Ray ray = targetRay;
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

        if (hit)
        {
            Debug.Log(hit.collider);
            foreach (Collider2D collider in adjacentBoxColliders)
            {
                if (hit.collider.gameObject == collider.gameObject)
                {
                    Vector2 emptyBoxCurrentPos = transform.position;
                    Vector2 emptyBoxFuturePos = collider.transform.position;

                    transform.position = emptyBoxFuturePos;
                    collider.transform.position = emptyBoxCurrentPos;

                    /*foreach (Collider2D collider2 in adjacentBoxColliders)
                    {
                        collider2.gameObject.GetComponent<SpriteRenderer>().color = new Color(collider2.gameObject.GetComponent<SpriteRenderer>().color.r,
                    collider2.gameObject.GetComponent<SpriteRenderer>().color.g,
                    collider2.gameObject.GetComponent<SpriteRenderer>().color.b,
                    1f);
                    }*/

                    canClearList = true;
                }
            }
        }

        if (canClearList)
        {
            adjacentBoxColliders.Clear();
        }
    }

    void SetAdjacentBoxes()
    {
        if (adjacentBoxColliders.Count == 0)
        {
            foreach (Collider2D collider in Physics2D.OverlapCircleAll(transform.position, 0.7f))
            {
                if (collider.gameObject != gameObject)
                {
                    adjacentBoxColliders.Add(collider);
                    if (!SwipeManager.instance.shouldSwipe)
                    {
                        collider.gameObject.GetComponent<SpriteRenderer>().color = new Color(collider.gameObject.GetComponent<SpriteRenderer>().color.r,
                        collider.gameObject.GetComponent<SpriteRenderer>().color.g,
                        collider.gameObject.GetComponent<SpriteRenderer>().color.b,
                        0.5f);
                    }
                }
            }
        }
    }
}