﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class VSModeManager : MonoBehaviour {

    public Transform canvas;
    public GameObject inBetweenScreen;
    public GameObject endScreen;
    public PlayableDirector _321Director;
    public PlayableDirector _321Prefab;

    public VSBoxManager boxManager;

    public Color winningTextColor;
    public GameObject leftSideConfetti;
    public float leftSideConfettiCoords;
    public GameObject rightSideConfetti;
    public float rightSideConfettiCoords;
    public float playerOneConfettiY;
    public float playerTwoConfettiY;

    [HideInInspector]
    public int playerIsDone;
    bool isInBetweenRounds = false;
    bool roundsAreOver = false;

    [HideInInspector]
    public TimeSpan player1Time;
    public TimeSpan player2Time;
    public TimeSpan winnerTime;

    bool canSearchForEndScript = false;

    //Not actually a search, just a flag
    bool canSearchForSecondAnim = false;

    GameObject rightConfetti;
    GameObject leftConfetti;

    private void Awake()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
        Mathf.Clamp(playerIsDone, 1, 2);

        PlayableDirector new321 = Instantiate(_321Prefab.gameObject, canvas).GetComponent<PlayableDirector>();
        _321Director = new321;
        //_321Director.transform.parent = canvas;
        _321Director.Play();
    }

    private void Update()
    {
        if (GameObject.FindGameObjectWithTag("321Panel"))
        {
            _321Director = GameObject.FindGameObjectWithTag("321Panel").GetComponent<PlayableDirector>();
        }

        if (!boxManager.readyToInit && playerIsDone >= 0)
        {
            if (_321Director != null)
            {
                if (_321Director.state != PlayState.Playing)
                {
                    boxManager.readyToInit = true;

                    Destroy(_321Director.gameObject);
                    _321Director = null;
                }
            }
        }

        if (playerIsDone == 1 && !isInBetweenRounds)
        {
            boxManager.hasInitiatedLevel = false;
            boxManager.readyToInit = false;
            boxManager.stopwatch.Stop();
            player1Time = boxManager.stopwatch.Elapsed;

            boxManager.ClearBoxes();
            Instantiate(inBetweenScreen, canvas);

            isInBetweenRounds = true;
        }

        if (playerIsDone == 2 && !roundsAreOver)
        {
            boxManager.stopwatch.Stop();
            player2Time = boxManager.stopwatch.Elapsed;

            Winner roundWinner = ChooseWinner();
            boxManager.ClearBoxes();

            GameObject newEndScreen = Instantiate(endScreen, canvas);

            Text player1ScoreText = newEndScreen.transform.Find("PlayerOneObjects").Find("PlayerOneScoreText").GetComponent<Text>();
            player1ScoreText.text = Math.Round(player1Time.TotalSeconds, 1).ToString();

            Text player2ScoreText = newEndScreen.transform.Find("PlayerTwoObjects").Find("PlayerTwoScoreText").GetComponent<Text>();
            player2ScoreText.text = Math.Round(player2Time.TotalSeconds, 1).ToString();

            //Display all digits if score is greater than 100
            if (player1Time.TotalSeconds > 100)
            {
                player1ScoreText.text = player1Time.TotalSeconds.ToString() + UnityEngine.Random.Range(1, 10).ToString();
                UnityEngine.Debug.Log("over 100");
            }
            else if (player2Time.TotalSeconds > 100)
            {
                player2ScoreText.text = player2Time.TotalSeconds.ToString() + UnityEngine.Random.Range(1, 10).ToString();
                UnityEngine.Debug.Log("over 100");
            }

            //Add a '.0' if the player's score is a whole number
            if (Math.Round(player1Time.TotalSeconds, 1) % 1 == 0)
            {
                player1ScoreText.text = Math.Round(player1Time.TotalSeconds, 1).ToString() + ".0";
            }
            else if (Math.Round(player2Time.TotalSeconds, 1) % 1 == 0)
            {
                player2ScoreText.text = Math.Round(player2Time.TotalSeconds, 1).ToString() + ".0";
            }

            UnityEngine.Debug.Log(roundWinner + ", " + winnerTime.Seconds);
            canSearchForEndScript = true;
            roundsAreOver = true;
        }

        if (canSearchForEndScript)
        {
            VSEndPanel endPanelScript = GameObject.FindGameObjectWithTag("EndPanel").GetComponent<VSEndPanel>();
            if (endPanelScript != null)
            {
                if (endPanelScript.initialAnimDirector.state != PlayState.Playing)
                {
                    Text player1ScoreText = endPanelScript.transform.Find("PlayerOneObjects").Find("PlayerOneScoreText").GetComponent<Text>();
                    Text player2ScoreText = endPanelScript.transform.Find("PlayerTwoObjects").Find("PlayerTwoScoreText").GetComponent<Text>();

                    //Change color of round winner's text to winner's color
                    if (ChooseWinner() == Winner.PlayerOne)
                    {
                        endPanelScript.transform.Find("PlayerOneObjects").Find("PlayerOneText").GetComponent<Text>().color = winningTextColor;
                        player1ScoreText.color = winningTextColor;
                        leftConfetti = Instantiate(leftSideConfetti, new Vector2(leftSideConfettiCoords, playerOneConfettiY), leftSideConfetti.transform.rotation);
                        rightConfetti = Instantiate(rightSideConfetti, new Vector2(rightSideConfettiCoords, playerOneConfettiY), rightSideConfetti.transform.rotation);
                        canSearchForEndScript = false;
                        canSearchForSecondAnim = true;
                    }
                    else
                    {
                        endPanelScript.transform.Find("PlayerTwoObjects").Find("PlayerTwoText").GetComponent<Text>().color = winningTextColor;
                        player2ScoreText.color = winningTextColor;
                        leftConfetti = Instantiate(leftSideConfetti, new Vector2(leftSideConfettiCoords, playerTwoConfettiY), leftSideConfetti.transform.rotation);
                        rightConfetti = Instantiate(rightSideConfetti, new Vector2(rightSideConfettiCoords, playerTwoConfettiY), rightSideConfetti.transform.rotation);
                        canSearchForEndScript = false;
                        canSearchForSecondAnim = true;
                    }

                    //Trigger winner anim
                }
            }
        }

        if (canSearchForSecondAnim)
        {
            VSEndPanel endPanelScript = GameObject.FindGameObjectWithTag("EndPanel").GetComponent<VSEndPanel>();

            //If confetti is done, trigger second anim.
            if (!leftConfetti.GetComponent<ParticleSystem>().isEmitting && !rightConfetti.GetComponent<ParticleSystem>().isEmitting)
            {
                endPanelScript.secondAnimDirector.Play();

                UnityEngine.Debug.Log("y");

                endPanelScript.playerOneText.SetActive(false);
                endPanelScript.playerTwoText.SetActive(false);

                Destroy(leftConfetti);
                Destroy(rightConfetti);

                canSearchForSecondAnim = false;
            }
        }
    }

    public void StartSecondRound()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas").transform;

        PlayableDirector new321 = Instantiate(_321Prefab.gameObject, canvas).GetComponent<PlayableDirector>();
        _321Director = new321;
        //_321Director.transform.parent = canvas;
        _321Director.transform.Find("PlayerText").GetComponent<Text>().text = "PLAYER TWO";
        _321Director.Play();
    }

    Winner ChooseWinner()
    {
        if (player1Time < player2Time)
        {
            winnerTime = player1Time;
            return Winner.PlayerOne;
        }

        else if (player2Time < player1Time)
        {
            winnerTime = player2Time;
            return Winner.PlayerTwo;
        }

        else
        {
            UnityEngine.Debug.Log("Couldn't choose winner!");
            return Winner.None;
        }
    }

    enum Winner
    {
        PlayerOne,
        PlayerTwo,
        None
    }
}
