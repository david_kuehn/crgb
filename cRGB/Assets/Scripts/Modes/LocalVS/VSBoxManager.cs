﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSBoxManager : MonoBehaviour {

    public GameObject solvedGroupPrefab;

    [Space]

    public GameObject emptyBoxPrefab;
    public GameObject redBoxPrefab;
    public GameObject greenBoxPrefab;
    public GameObject blueBoxPrefab;

    [Space]

    int redBoxCount = 0, greenBoxCount = 0, blueBoxCount = 0;

    [Header("Dynamic Box Positions")]
    public List<GameObject> dynamicBoxPositions;

    [Header("Solution Box Positions")]
    public List<GameObject> solutionBoxPositions;

    [Header("Box Coordinates")]
    public List<Vector2> boxCoordinates;

    private bool dynamicsHaveBeenSet = false;

    [Header("Solution Renderer")]
    public int columns;
    public int boxesInColumn;
    public float downscaleFactor;

    public float ySpacing = .6f;

    public float[] startingXPositions;
    public float startingYPosition = -3.2f;

    public bool hasInitiatedLevel = false;
    public bool readyToInit = false;

    public enum CurrentPlayer { PlayerOne, PlayerTwo }

    public Stopwatch stopwatch = new Stopwatch();

    private void Update()
    {
        if (!hasInitiatedLevel && readyToInit)
        {
            GenerateBoxes();

            RenderSolution();

            ThemeManager.instance.SetThemeLevelProperties(dynamicBoxPositions);

            stopwatch.Start();

            hasInitiatedLevel = true;
        }

        if (hasInitiatedLevel && readyToInit)
        {
            UpdateDynamicBoxes();
        }
    }

    public void GenerateBoxes()
    {
        //Generate empty box
        GameObject newEmptyBox = Instantiate(emptyBoxPrefab, this.transform);
        newEmptyBox.AddComponent<VSBoxMovement>();

        dynamicBoxPositions.Add(newEmptyBox);
        solutionBoxPositions.Add(newEmptyBox);

        //If the numbers of boxes haven't already been set
        if (redBoxCount == 0)
        {
            //Generate red, green, blue boxes
            redBoxCount = UnityEngine.Random.Range(1, 7);       //1 and 7 because one box in grid is empty, so only 8 colored boxes are needed
            greenBoxCount = UnityEngine.Random.Range(1, 8 - (redBoxCount + 1));     //(redBoxCount - 1) because there needs to be at least one space for blue
            blueBoxCount = 8 - (redBoxCount + greenBoxCount);
        }

        for (int i = 0; i < redBoxCount; i++)
        {
            GameObject newRedBox = Instantiate(redBoxPrefab, this.transform);
            dynamicBoxPositions.Add(newRedBox);
            solutionBoxPositions.Add(newRedBox);
        }

        for (int i = 0; i < greenBoxCount; i++)
        {
            GameObject newGreenBox = Instantiate(greenBoxPrefab, this.transform);
            dynamicBoxPositions.Add(newGreenBox);
            solutionBoxPositions.Add(newGreenBox);
        }

        for (int i = 0; i < blueBoxCount; i++)
        {
            GameObject newBlueBox = Instantiate(blueBoxPrefab, this.transform);
            dynamicBoxPositions.Add(newBlueBox);
            solutionBoxPositions.Add(newBlueBox);
        }

        //Shuffle solution, then shuffle dynamics
        ShuffleBoxes(solutionBoxPositions);
        ShuffleBoxes(dynamicBoxPositions);

        //Put dynamic boxes in position
        AssignPositionToBoxes();

        dynamicsHaveBeenSet = true;
    }

    public void ShuffleBoxes(List<GameObject> listToShuffle)
    {
        for (int i = 0; i < listToShuffle.Count; i++)
        {
            GameObject temp = listToShuffle[i];
            int randomIndex = UnityEngine.Random.Range(i, listToShuffle.Count);
            listToShuffle[i] = listToShuffle[randomIndex];
            listToShuffle[randomIndex] = temp;
        }
    }

    public void AssignPositionToBoxes()
    {
        for (int i = 0; i < dynamicBoxPositions.Count; i++)
        {
            dynamicBoxPositions[i].transform.position = boxCoordinates[i];
        }
    }

    public void UpdateDynamicBoxes()
    {
        if (dynamicsHaveBeenSet)
        {
            for (int i = 0; i < dynamicBoxPositions.Count; i++)
            {
                Ray ray = new Ray(new Vector3(boxCoordinates[i].x, boxCoordinates[i].y, -1), Vector3.back);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

                dynamicBoxPositions[i] = hit.collider.gameObject;
            }
        }
    }

    public bool CheckIfSolved()
    {
        //for (int i = 0; i < solutionBoxPositions.Count; i++)
        //{
        //    if (dynamicBoxPositions[i].GetComponent<SpriteRenderer>().color != solutionBoxPositions[i].GetComponent<SpriteRenderer>().color)
        //    {
        //        return false;
        //    }
        //}
        //return true;

        
        for (int i = 0; i < solutionBoxPositions.Count; i++)
        {
            //Makes a string of the first four characters of the name of the Box currently being iterated (essentially getting the color of the box)
            string firstFourCharactersString = solutionBoxPositions[i].name.ToCharArray(0, 4)[0].ToString() +
                solutionBoxPositions[i].name.ToCharArray(0, 4)[1].ToString() +
                solutionBoxPositions[i].name.ToCharArray(0, 4)[2].ToString() +
                solutionBoxPositions[i].name.ToCharArray(0, 4)[3].ToString();

            //If the corresponding Dynamic Box doesn't contain the name of the current Solution Box, then return false
            if (!dynamicBoxPositions[i].name.Contains(firstFourCharactersString))
            {
                return false;
            }
        }

        return true;
    }

    private void RenderSolution()
    {
        GameObject solutionParent = new GameObject("SolutionParent");
        solutionParent.transform.parent = this.transform;

        List<GameObject> solutionBoxes = new List<GameObject>();

        int currentColumn = 0;

        for (int i = 0; i < columns; i++)
        {
            for (int x = 0; x < boxesInColumn; x++)
            {
                GameObject objectJustInstantiated = Instantiate(solutionBoxPositions[x + (i * boxesInColumn)], solutionParent.transform);
                objectJustInstantiated.transform.localScale = new Vector3(objectJustInstantiated.transform.localScale.x * downscaleFactor, objectJustInstantiated.transform.localScale.y * downscaleFactor, 1);
                objectJustInstantiated.transform.position = new Vector3(startingXPositions[currentColumn], startingYPosition + (ySpacing * -x));

                solutionBoxes.Add(objectJustInstantiated);

                if (objectJustInstantiated.tag == "EmptyBox")
                {
                    Destroy(objectJustInstantiated.GetComponent<VSBoxMovement>());
                    Destroy(objectJustInstantiated.GetComponent<BoxCollider2D>());

                    objectJustInstantiated.tag = "Untagged";
                }

                if (objectJustInstantiated.tag == "ColorBox")
                {
                    Destroy(objectJustInstantiated.GetComponent<BoxCollider2D>());

                    objectJustInstantiated.tag = "Untagged";
                }
            }

            currentColumn += 1;
        }

        ThemeManager.instance.SetThemeLevelProperties(solutionBoxes);
    }

    public void ClearBoxes()
    {
        dynamicBoxPositions.Clear();
        solutionBoxPositions.Clear();

        stopwatch.Reset();

        foreach (Transform transf in gameObject.transform)
        {
            Destroy(transf.gameObject);
        }
    }
}
