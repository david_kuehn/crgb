﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{
    #region Singleton
    public static AdManager instance = null;

    public int levelsUntilShowAd;
    private int levelsLoadedInInstance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        levelsLoadedInInstance = 0;
    }
    #endregion

    public int coinsAwardedForWatchingAd;

    private enum AdPlacementType
    {
        Skippable,
        Rewarded
    }

    private AdPlacementType currentAdType;

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            currentAdType = AdPlacementType.Rewarded;
        }
    }

    public void ShowSkippableAd()
    {
        if (Advertisement.IsReady("video"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("video", options);
            currentAdType = AdPlacementType.Skippable;
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");

                if (currentAdType == AdPlacementType.Rewarded)
                {
                    CurrencyManager.instance.AddCoins(coinsAwardedForWatchingAd);
                    WatchAdTimerManager.instance.StartAdTimersAtDefaultLength();
                }

                break;

            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;

            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    public void AddToOrTriggerAd()
    {
        levelsLoadedInInstance += 1;

        if (levelsLoadedInInstance % levelsUntilShowAd == 0)
        {
            ShowSkippableAd();
            levelsLoadedInInstance = 0;
        }
    }
}
