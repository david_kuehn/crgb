﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeManager : MonoBehaviour {
    #region Singleton
    public static SwipeManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        shouldSwipe = true;
    }
    #endregion

    public bool shouldSwipe;

    [Range(0, 1)]
    public float deadZoneRadius;

    Vector2 touchStartPosition;
    Vector2 touchEndPosition;
    bool touchStartRecorded = false;
    bool touchEndRecorded = false;

    public SwipeDirection InGameSwipe()
    {
        Touch[] touchesArray = Input.touches;

        if (Input.touchCount > 0)
        {
            if (touchesArray[0].phase == TouchPhase.Began)
            {
                touchStartPosition = Camera.main.ScreenToWorldPoint(touchesArray[0].position);
                touchStartRecorded = true;
            }

            else if (touchesArray[0].phase == TouchPhase.Ended || touchesArray[0].phase == TouchPhase.Canceled)
            {
                touchEndPosition = Camera.main.ScreenToWorldPoint(touchesArray[0].position);
                touchEndRecorded = true;
            }
        }

        if (touchStartRecorded && touchEndRecorded)
        {
            Vector2 touchDelta = touchEndPosition - touchStartPosition;

            if (touchDelta.magnitude > deadZoneRadius)
            {
                if (Mathf.Abs(touchDelta.x) > Mathf.Abs(touchDelta.y))
                {
                    if (touchDelta.x > 0)
                    {
                        ResetValues();
                        return SwipeDirection.SwipeRight;
                    }

                    else
                    {
                        ResetValues();
                        return SwipeDirection.SwipeLeft;
                    }
                }

                else
                {
                    if (touchDelta.y > 0)
                    {
                        ResetValues();
                        return SwipeDirection.SwipeUp;
                    }
                    else
                    {
                        ResetValues();
                        return SwipeDirection.SwipeDown;
                    }
                }
            }

            else
            {
                ResetValues();
            }
        }

        return SwipeDirection.NoSwipe;
    }

    void ResetValues()
    {
        touchStartPosition = Vector2.zero;
        touchEndPosition = Vector2.zero;
        touchStartRecorded = touchEndRecorded = false;
    }

}

public enum SwipeDirection
{
    SwipeUp, SwipeDown, SwipeLeft, SwipeRight, NoSwipe
}
