﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewFeaturePanelSize : MonoBehaviour {

    public GameObject panel, text;

    public Vector2 paddingXY;

    private void Awake()
    {
        panel.GetComponent<RectTransform>().sizeDelta = new Vector2(text.GetComponent<RectTransform>().sizeDelta.x * paddingXY.x, text.GetComponent<RectTransform>().sizeDelta.y - paddingXY.y);
    }
}
