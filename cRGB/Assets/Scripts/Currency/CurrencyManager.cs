﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CurrencyManager : MonoBehaviour {

    #region Singleton
    public static CurrencyManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    #endregion

    public int coins;

    public TextMeshProUGUI amountText;

    private void Start()
    {
        Mathf.Clamp(coins, 0, 100000000);
        coins = PlayerPrefs.GetInt("Coins");
    }

    public void AddCoins(int coinsToAdd)
    {
        coins += coinsToAdd;

        PlayerPrefs.SetInt("Coins", coins);
    }

    public void SubtractCoins(int coinsToSubtract)
    {
        coins -= coinsToSubtract;

        PlayerPrefs.SetInt("Coins", coins);
    }

    public void UpdateAmountOfCoins()
    {
        GameObject amountDisplayObject = GameObject.FindGameObjectWithTag("CoinAmountDisplay");
        if (amountDisplayObject != null)
        {
            amountText = amountDisplayObject.GetComponent<TextMeshProUGUI>();

            amountText.text = coins.ToString();
        }
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("Coins", coins);
    }
}
