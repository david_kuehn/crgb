﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThemeShop : MonoBehaviour
{
    public GameObject buttonPrefab;
    public GameObject contentParent;

    private List<GameObject> themeButtons = new List<GameObject>();

    ThemeManager themeManagerScript = ThemeManager.instance;

    private void Start()
    {
        InitializeThemeButtons();
    }

    private void Update()
    {
        contentParent.GetComponent<Image>().color = Camera.main.backgroundColor;
    }

    public void RequestToBuyTheme(string themeToBuy)
    {
        foreach (Theme theme in themeManagerScript.themes)
        {
            if (theme.themeName == themeToBuy)
            {
                if (CurrencyManager.instance.coins >= theme.price)
                {
                    themeManagerScript.purchasedThemes.Add(theme);
                    LevelSaveManager.SavePurchasedThemes(Application.persistentDataPath + "/StreamingAssets/ThemeData/");

                    CurrencyManager.instance.SubtractCoins(theme.price);

                    UpdateThemeButtons();
                }
            }
        }
    }

    private void InitializeThemeButtons()
    {
        foreach (Theme theme in themeManagerScript.themes)
        {
            if (theme.themeName != "Default")
            {
                GameObject instantiatedButton = Instantiate(buttonPrefab, contentParent.transform);
                instantiatedButton.name = theme.themeName + " Button";
                instantiatedButton.transform.Find("ThemeNameText").GetComponent<Text>().text = theme.themeName;
                instantiatedButton.transform.Find("PriceText").GetComponent<Text>().text = theme.price.ToString();

                instantiatedButton.GetComponent<Button>().onClick.AddListener(() => RequestToBuyTheme(theme.themeName));

                themeButtons.Add(instantiatedButton);
            }
        }

        UpdateThemeButtons();
    }

    private void UpdateThemeButtons()
    {
        bool hasFlaggedAsNotInteractable = false;

        Debug.Log(themeButtons.Count);
        foreach (GameObject button in themeButtons)
        {
            foreach (Theme purchasedTheme in themeManagerScript.purchasedThemes)
            {
                if (button.name.Contains(purchasedTheme.themeName))
                {
                    button.GetComponent<Button>().interactable = false;

                    hasFlaggedAsNotInteractable = true;

                    Debug.Log(button.name + button.GetComponent<Button>().interactable);
                }
            }

            foreach (Theme theme in themeManagerScript.themes)
            {
                if (button.name.Contains(theme.themeName))
                {
                    if (theme.price > CurrencyManager.instance.coins)
                    {
                        button.GetComponent<Button>().interactable = false;

                        hasFlaggedAsNotInteractable = true;

                        Debug.Log(button.name + button.GetComponent<Button>().interactable);
                    }
                }
            }

            if (!hasFlaggedAsNotInteractable)
            {
                button.GetComponent<Button>().interactable = true;
            }
        }
    }
}
