﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelGenerator : EditorWindow {

    Vector2 levelSize;

    public GameObject redBoxPrefab;
    public GameObject greenBoxPrefab;
    public GameObject blueBoxPrefab;
    public GameObject emptyBoxPrefab;

    [MenuItem("Window/Level Generator")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<LevelGenerator>("Level Generator");
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        redBoxPrefab = EditorGUILayout.ObjectField("Red Box Prefab", redBoxPrefab, typeof(GameObject), false, null) as GameObject;
        greenBoxPrefab = EditorGUILayout.ObjectField("Green Box Prefab", greenBoxPrefab, typeof(GameObject), false, null) as GameObject;
        blueBoxPrefab = EditorGUILayout.ObjectField("Blue Box Prefab", blueBoxPrefab, typeof(GameObject), false, null) as GameObject;
        emptyBoxPrefab = EditorGUILayout.ObjectField("Empty Box Prefab", emptyBoxPrefab, typeof(GameObject), false, null) as GameObject;
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        levelSize = EditorGUILayout.Vector2Field("Level Size", levelSize);

        if (GUILayout.Button("Generate Level"))
        {
            Debug.Log("This button does nothing right now.");

            //GenerateLevel();
        }
    }

    /*void GenerateLevel()
    {
        for (int x = 0; x < levelSize.x; x++)
        {
            for (int y = 0; y < levelSize.y; y++)
            {
                if (Random.Range(0, 3) == 0)
                {
                    Instantiate(redBoxPrefab);
                }
            }
        }
    }*/
}
