﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoxManager))]
[CanEditMultipleObjects]
public class BoxManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BoxManager boxManagerScript = target as BoxManager;

        EditorGUILayout.Space();

        GUILayout.BeginVertical();

        if (GUILayout.Button("Assign References to Boxes"))
        {
            GameObject[] boxesInScene = GameObject.FindGameObjectsWithTag("ColorBox");

            for (int i = 0; i < boxesInScene.Length; i++)
            {
                boxManagerScript.dynamicBoxPositions[i] = boxesInScene[i];
            }
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Randomize Dynamic Values"))
        {
            for (int i = 0; i < boxManagerScript.dynamicBoxPositions.Count; i++)
            {
                GameObject temp = boxManagerScript.dynamicBoxPositions[i];
                int randomIndex = Random.Range(i, boxManagerScript.dynamicBoxPositions.Count);
                boxManagerScript.dynamicBoxPositions[i] = boxManagerScript.dynamicBoxPositions[randomIndex];
                boxManagerScript.dynamicBoxPositions[randomIndex] = temp;
            }
        }

        if (GUILayout.Button("Update Dynamic Values"))
        {
            for (int i = 0; i < boxManagerScript.solutionBoxPositions.Count; i++)
            {
                boxManagerScript.dynamicBoxPositions[i].transform.position = boxManagerScript.boxCoordinates[i];
            }
        }

        if (GUILayout.Button("Copy Dynamics into Solutions"))
        {
            for (int i = 0; i < boxManagerScript.solutionBoxPositions.Count; i++)
            {
                boxManagerScript.solutionBoxPositions[i] = boxManagerScript.dynamicBoxPositions[i];
            }
        }

        if (GUILayout.Button("Copy Solutions into Dynamics"))
        {
            for (int i = 0; i < boxManagerScript.dynamicBoxPositions.Count; i++)
            {
                boxManagerScript.dynamicBoxPositions[i] = boxManagerScript.solutionBoxPositions[i];
            }
        }

        GUILayout.EndVertical();
    }
}
