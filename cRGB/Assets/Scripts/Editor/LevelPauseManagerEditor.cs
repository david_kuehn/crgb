﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelPauseManager))]
[CanEditMultipleObjects]
public class LevelPauseManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

#pragma warning disable 0219
        LevelPauseManager levelPauseManagerScript = target as LevelPauseManager;
#pragma warning restore 0219

        EditorGUILayout.Space();


        /*if (GUILayout.Button("Next Level"))
        {
            levelPauseManagerScript.LoadNextLevel();
        }*/
    }
}
